﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShot : MonoBehaviour  {

  //public Camera camera;
  private Texture2D ScreenShot;
  private RenderTexture rt;
  private int resWidth = 1920;
  private int resHeight = 1080;

  // Use this for initialization
  void Start()
  {
    rt = new RenderTexture(resWidth, resHeight, 24);
    ScreenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
  }

  // Update is called once per frame
  void Update()
  {

  }

   public void PrintSceen(Camera camera,string savepath)
  {
    camera.targetTexture = rt;
    camera.Render();
    RenderTexture.active = rt;

    ScreenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);

    RenderTexture.active = null; //can help avoid errors 
    camera.targetTexture = null;

    var bytes = ScreenShot.EncodeToPNG();
    System.DateTime now = System.DateTime.Now;
    string date = now.Year + "-" + now.Month + "-" + now.Day + "-" + now.Hour + "-" + now.Minute + "-" + now.Second;
    string FILE_NAME = "BandData-" + date + ".png";
    System.IO.File.WriteAllBytes(savepath + "/" + FILE_NAME, bytes);
    Debug.Log(Application.persistentDataPath);
  }
}
