﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MyPath
{

  static public string GetStreamingAssetsPath(string filename)//注意附檔名
  {
    string filepath = "";

#if UNITY_EDITOR 
       filepath = Path.Combine(Application.streamingAssetsPath, filename);

#endif

#if UNITY_ANDROID // Android 必須使用WWW才能拿到StreamingAssets內的file
      filepath = "jar:file://" + Application.dataPath + "!/assets/" + filename;
      WWW wwwfile = new WWW(filepath);
      while (!wwwfile.isDone) { }
       filepath = string.Format("{0}/{1}", Application.persistentDataPath, filename);
    File.WriteAllBytes(filepath, wwwfile.bytes);
     
#endif

    return filepath;
  }

}
