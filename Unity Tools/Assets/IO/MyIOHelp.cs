﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MyIOHelp
{
  public class MyJson
  {
    public static void SaveToJson<T>(string savepath, T[] array, string filename)
    {
      checkSavePathIsExists(savepath);

      string data = arrayToJson<T>(array);

      string destinationPath = Path.Combine(savepath, filename);

      WriteDATA(data, destinationPath);

    }

    //Json沒辦法直接對array儲存
    //支援把array轉成Json格式儲存
    //Usage:
    //YouObject[] objects = JsonHelper.getJsonArray<YouObject> (jsonString);
    static T[] getJsonArray<T>(string json)
    {
      string newJson = "{ \"array\": " + json + "}";
      Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
      return wrapper.array;
    }
    //Usage:
    //string jsonString = JsonHelper.arrayToJson<YouObject>(objects);
    static string arrayToJson<T>(T[] array)
    {
      Wrapper<T> wrapper = new Wrapper<T>();
      wrapper.array = array;
      return JsonUtility.ToJson(wrapper);
    }

    [Serializable]
    private class Wrapper<T>
    {
      public T[] array;
    }
  }
  public class MYCSV
  {
    static public void SaveToCSV<T>(string savepath,T[] array, string filename)
    {
      checkSavePathIsExists(savepath);

      String destinationPath = Path.Combine(savepath, filename);

      string data = "";
      
      foreach (T d in array)
      {
        //自定義想要的格式
        //data += d.Level + "," + d.DATA1 + "," + d.DATA2 + "," + d.DATA3 + "\n"; 
      }

      WriteDATA(data, destinationPath + ".csv");
    }
  }

  static void checkSavePathIsExists(string savepath)
  {
    //如果savepath不存在就create一個
    if (!Directory.Exists(savepath))
    {
      Debug.Log("save path : " + savepath + "，路徑不存在..CreatePath....");
      Directory.CreateDirectory(savepath);
    }
  }

  static void Log(string info)
  {
    Debug.Log(info);
  }

  static void LogWarning(string info)
  {
    Debug.LogWarning(info);
  }

  static void LogException(Exception e)
  {
    Debug.LogException(e);
  }

  static void WriteDATA(string data,string fullpath)
  {
    try
    {
      StreamWriter sw = new StreamWriter(fullpath, true, System.Text.Encoding.Default);

      sw.Write(data);
      sw.Dispose();
      sw.Close();
      Log("存檔成功，路徑 : " + fullpath);
    }
    catch (Exception e)
    {
      LogException(e);
    }
  }
}
